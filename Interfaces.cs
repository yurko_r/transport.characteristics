using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace transport.characteristics
{
    class Program
    {
        static void Main(string[] args)
        {
            Transport bus = new Transport(8, "public", 50, "Bus", 223);
            bus.Characteristics();
            Console.WriteLine();

            Transport car = new Transport(4, "individual", 5, "Car", 112);
            car.Characteristics();
            Console.WriteLine();
            
            Transport bike = new Transport(2, "individual", 1, "Bike", 331);
            bike.Characteristics();
            Console.WriteLine();

            Console.ReadKey();
        }
    }
}