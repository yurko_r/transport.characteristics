using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace transport.characteristics
{
    public class Transport : ITransportCharacteristics
    {
        int wheels;
        string type;
        int passenger;
        string name;
        int vin;
        
        public Transport (int wls, string tp, int pgr, string nm, int vin1)
        {
            wheels = wls;
            type = tp;
            passenger = pgr;
            name = nm;
            vin = vin1;
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Type
        {
            get { return type;}
            set { type = value;}
        }

        public int Passengers
        {
            get { return passenger; }
            set { passenger = value; }
        }
        public int Wheels
        {
            get { return wheels; }
            set { wheels = value; }
        }

        public int Vin
        {
            get { return vin; }
            set { vin = value; }
        }

        public void Characteristics()
        {
            Console.WriteLine("Transport Characteristics:");
            Console.WriteLine("Name of Transport: {0}", Name);
            Console.WriteLine("Type of Transport: {0}", Type);
            Console.WriteLine("Number of Passengers:  {0}", Passengers);
            Console.WriteLine("Number of Wheels:  {0}", Wheels);
            Console.WriteLine("VIN:  {0}", Vin);
        }
    }
}
